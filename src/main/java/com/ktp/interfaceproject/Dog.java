/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.interfaceproject;

/**
 *
 * @author acer
 */
public class Dog extends LandAnimal{
    public String name;
    
    public Dog() {
        super("Dog : ",4);
        this.name = name;
    }

    public Dog(String name, int numberOfLeg) {
        super(name, numberOfLeg);
        this.name = name;
    }

    @Override
    public void eat() {
        System.out.println("Dog : "+ name + "eat");
    }

    @Override
    public void speak() {
        System.out.println("Dog : "+ name + "speak");
    }

    @Override
    public void sleep() {
        System.out.println("Dog : "+ name + "sleep");
    }

    @Override
    public void run() {
        System.out.println("Dog : "+ name + "run");
    }
    
}
