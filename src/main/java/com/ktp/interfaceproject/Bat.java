/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.interfaceproject;

/**
 *
 * @author acer
 */
public class Bat extends Poultry{
    public String name;

    public Bat() {
        super("Bat : ",2);
        this.name = name;
    }

    public Bat(String name, int numberOfLeg) {
        super(name, numberOfLeg);
        this.name = name;
    }
    

    @Override
    public void eat() {
        System.out.println("Bat : "+ name + "eat");
    }

    @Override
    public void speak() {
        System.out.println("Bat : "+ name + "speak");
    }

    @Override
    public void sleep() {
       System.out.println("Bat : "+ name + "sleep");
    }

    @Override
    public void fly() {
        System.out.println("Bat : "+ name + "fly");
    }
    
}
