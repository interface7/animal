/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.interfaceproject;

/**
 *
 * @author acer
 */
public class Fish extends AquaticAnimal{
    public String name;
    
    public Fish(String name) {
        super("Fish", 0);
        this.name = name;
    }

    public Fish(String name, int numberOfLeg) {
        super(name, numberOfLeg);
        this.name = name;
    }
    

    @Override
    public void eat() {
        System.out.println("Fish: "+ name + "eat");
    }

    @Override
    public void speak() {
        System.out.println("Fish: "+ name + "speak");
    }

    @Override
    public void sleep() {
        System.out.println("Fish: "+ name + "sleep");
    }

    @Override
    public void swim() {
        System.out.println("Fish: "+ name + "swim");
    }
    
}
