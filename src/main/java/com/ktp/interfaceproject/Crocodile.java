/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.interfaceproject;

/**
 *
 * @author acer
 */
public class Crocodile extends Reptile{
    public String name;
    
    public Crocodile(String name) {
        super("Crocodile", 4);
        this.name = name;
    }

    public Crocodile(String name, int numberOfLeg) {
        super(name, numberOfLeg);
        this.name = name;
    }

    @Override
    public void eat() {
        System.out.println("Crocodile: "+ name + "eat");
    }

    @Override
    public void speak() {
        System.out.println("Crocodile: "+ name + "speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crocodile: "+ name + "sleep");
    }

    @Override
    public void crawl() {
        System.out.println("Crocodile: "+ name + "crawl");
    }
    
    
}
