/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.interfaceproject;

/**
 *
 * @author acer
 */
public class TestFlyable {

    public static void main(String[] args) {
        Bat bat = new Bat("lim ", 2);
        Bird bird = new Bird("bi ", 2);
        Plane plane = new Plane("Engine number 1");
        bat.eat();
        bat.sleep();
        bat.fly();
        plane.fly();
        bird.fly();
        bird.eat();
        bird.sleep();
        Dog dog = new Dog("Tae ", 4);
        Car car = new Car("Engine number 2");
        Cat cat = new Cat("Yu ", 4);
        Human human = new Human("Nan ", 2);
        dog.eat();
        dog.sleep();
        dog.speak();
        dog.run();
        cat.run();
        cat.eat();
        cat.sleep();
        cat.speak();
        human.run();
        human.eat();
        human.sleep();
        human.speak();
        car.run();
        Fish fish = new Fish("hae ", 0);
        Crab crab = new Crab("smile ", 8);
        Boat boat = new Boat("Engine number 2");
        fish.eat();
        fish.sleep();
        fish.swim();
        crab.eat();
        crab.sleep();
        crab.swim();
        boat.swim();
        Crocodile crocodile = new Crocodile("jae ",4);
        Snake snake = new Snake("Tim ",0);
        snake.eat();
        snake.sleep();
        snake.crawl();
        crocodile.eat();
        crocodile.sleep();
        crocodile.crawl();

        Flyable[] flyable = {bat, bird, plane};
        for (Flyable f : flyable) {
            if (f instanceof Plane) {
                Plane p = (Plane) f;
                p.startengine();
                p.run();
            }
            f.fly();
        }

        Runable[] runable = {dog, plane, car};
        for (Runable r : runable) {
            r.run();
        }

        Swimable[] swimable = {fish, crab, boat};
        for (Swimable s : swimable) {
            s.swim();
        }
        
        Crawlable[] crawlable = {crocodile,snake};
        for (Crawlable c : crawlable) {
            c.crawl();
        }       
    }
}
