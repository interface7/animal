/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.interfaceproject;

/**
 *
 * @author acer
 */
public class Car extends Vahicle implements Runable{
    
    public Car(String engine) {
        super(engine);
    }

    @Override
    public void startengine() {
        System.out.println("Car : startengine");
    }

    @Override
    public void stopengine() {
        System.out.println("Car : stopengine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Car : raiseSpeed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Car : applyBreak");
    }

    @Override
    public void run() {
        System.out.println("Car : run");
    }
    
}
