/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.interfaceproject;

/**
 *
 * @author acer
 */
public class Human extends LandAnimal{
    public String name;
    
    public Human () {
        super("Human : ",2);
        this.name = name;
    }

    public Human(String name, int numberOfLeg) {
        super(name, numberOfLeg);
        this.name = name;
    }
    

    @Override
    public void eat() {
        System.out.println("Human  : "+ name + "eat");
    }

    @Override
    public void speak() {
        System.out.println("Human  : "+ name + "speak");
    }

    @Override
    public void sleep() {
        System.out.println("Human  : "+ name + "sleep");
    }

    @Override
    public void run() {
        System.out.println("Human  : "+ name + "run");
    }
    
}
