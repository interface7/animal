/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.interfaceproject;

/**
 *
 * @author acer
 */
public class Boat extends Vahicle implements Swimable{

    public Boat(String engine) {
        super(engine);
    }

    @Override
    public void startengine() {
        System.out.println("Boat : startengine");
    }

    @Override
    public void stopengine() {
        System.out.println("Boat : stopengine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Boat : raiseSpeed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Boat : applyBreak");
    }

    @Override
    public void swim() {
        System.out.println("Boat : swim");
    }
    
}
