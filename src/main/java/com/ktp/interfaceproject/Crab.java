/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ktp.interfaceproject;

/**
 *
 * @author acer
 */
public class Crab extends AquaticAnimal{
    public String name;
    
    public Crab(String name) {
        super("Crab", 8);
        this.name = name;
    }

    public Crab(String name, int numberOfLeg) {
        super(name, numberOfLeg);
        this.name = name;
    }
    

    @Override
    public void eat() {
        System.out.println("Crab: "+ name + "eat");
    }

    @Override
    public void speak() {
        System.out.println("Crab: "+ name + "speak");
    }

    @Override
    public void sleep() {
        System.out.println("Crab: "+ name + "sleep");
    }

    @Override
    public void swim() {
        System.out.println("Crab: "+ name + "swim");
    }
}
